$(document).ready(function () {
    AOS.init({
        offset: 200,
        duration: 600,
        easing: 'ease-out',
        delay: 100
    });
    $('.navigation__link, .navigation__buy-button').click(function () {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 160
        }, 400);
        return false;
    });
    $('.scrolltop a').animate({scrollTop: 0}, 400);

    $(window).scroll(function(){
        if ($(window).scrollTop() >= 170) {
            $('nav').addClass('fixed-header');
        }
        else {
            $('nav').removeClass('fixed-header');
        }
    });
    $('section').mouseenter(function() {
        $('.navigation__link[href="#'+$(this).attr('id')+'"]').addClass('active').parent('li').siblings().find('.navigation__link').removeClass('active');
    });


    var specification_skew_background = {
        red: "207, 85, 81",
        yellow: "241, 239, 142",
        green: "137, 192, 105",
        black: "41, 41, 41",
        blue: "100, 183, 205",
        pink: "237, 194, 209"
    };
    $('.color-selector__circle').click(function () {
        $('.color-selector__circle').removeClass("active");
        $(this).addClass('active');
        var data_color = $(this).attr('data-color'),
            change_phone_src = $('.changeable-phone').attr('src').split('/'),
            change_phone_new_src;
        change_phone_src[change_phone_src.length - 1] = data_color + "_phone.png";
        change_phone_new_src = change_phone_src.join('/');
        $('.changeable-phone').attr('src', change_phone_new_src);
        $('.specification__content .text-wrapper__skew').css('background', "rgba(" + specification_skew_background[data_color] +", 0.9)");
        $('.specification .text-wrapper__heading').css('color', "rgb(" + specification_skew_background[data_color] +")");
    });
    $(".specification__button button").click(function () {
        $(this).parents(".toggable").fadeOut(200);
        $(this).parents(".toggable").siblings().fadeIn(200);
    });
});


var modal = document.getElementById('myModal');
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("discuss-modal__close")[0];
btn.onclick = function () {
    modal.style.display = "block";
};
span.onclick = function () {
    modal.style.display = "none";
};
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};